$(document).ready(function () {
    $('button.read-more').click(function (e) {
        $(this).parent().toggleClass('expanded')
    })


    //08. PROGRAMACIÓN MENU (Anclas, clickear fuera y abrir menu)

    $('a[href^="#"]').on('click', function (e) {
        var id = $(this).attr('href');

        // target element
        var $id = $(id);
        if ($id.length === 0) {
            return;
        }

        // prevent standard hash navigation (avoid blinking in IE)
        e.preventDefault();

        // top position relative to the document
        var pos = $id.offset().top - 200;

        // animated top scrolling
        $('body, html').animate({scrollTop: pos});
    });


    $(document).ready(function () {
        $(".menu_icon").click(function () {
            $(".arrowMenu").toggle(50);
            $(".menuContainer").toggle(50);
        });
    });

    $(document).scroll(function () {
        $(".arrowMenu").hide(50);
        $(".menuContainer").hide(50);
    });

    $('.menu li a').click(function () {
        $(".arrowMenu").toggle(50);
        $(".menuContainer").toggle(50);
    })

});